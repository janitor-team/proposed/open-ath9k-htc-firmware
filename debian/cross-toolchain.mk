# Copyright (c) 2016 Aurelien Jarno <aurelien@aurel32.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This makefile allows to quickly build minimalistic cross-compilers
# for various targets. They only support the C language and do not
# support a C library.
#
# Most of the code was borrowed from the openbios Debian source package.
# Changes made in 2020 by John Scott <jscott@posteo.net>

include /usr/share/dpkg/buildopts.mk
DEB_BUILD_OPTION_PARALLEL ?= 1

gcc_major_version    != sed -En 's/.*gcc-(.*)-source.*/\1/p' debian/control

target                = $(filter-out %_,$(subst _,_ ,$@))
toolchain_dir         = $(CURDIR)/cross-toolchain
stamp                 = $(toolchain_dir)/stamp-
binutils_src_dir      = /usr/src/binutils
binutils_unpack_dir   = $(toolchain_dir)/binutils-source
binutils_build_dir    = $(toolchain_dir)/binutils-$(target)
gcc_src_dir           = /usr/src/gcc-$(gcc_major_version)
gcc_unpack_dir        = $(toolchain_dir)/gcc-source
gcc_build_dir         = $(toolchain_dir)/gcc-$(target)

# Use only xtensa-specific patches on top of upstream version
$(stamp)binutils_unpack:
	mkdir -p $(binutils_unpack_dir)
	cd $(binutils_unpack_dir) && \
		tar --strip-components=1 -xf $(binutils_src_dir)/binutils-*.tar.* && \
		patch -p1 < $(CURDIR)/local/patches/binutils-2.36_fixup.patch && \
		patch -p1 < $(CURDIR)/local/patches/binutils-2.34_fixup.patch && \
		patch -p1 < $(CURDIR)/local/patches/binutils.patch && \
		rm -rf ./zlib/
	touch $@

$(stamp)binutils_%: $(stamp)binutils_unpack
	mkdir -p $(binutils_build_dir)
	cd $(binutils_build_dir) && \
		$(binutils_unpack_dir)/configure \
			--build=$(DEB_BUILD_GNU_TYPE) \
			--host=$(DEB_BUILD_GNU_TYPE) \
			--target=$(target) \
			--prefix=$(toolchain_dir) \
			--disable-nls \
			--disable-plugins \
			--with-system-zlib \
			--enable-build-warnings
	$(MAKE) -C $(binutils_build_dir) -j$(DEB_BUILD_OPTION_PARALLEL) all
	$(MAKE) -C $(binutils_build_dir) install
	touch $@

$(stamp)gcc_unpack:
	mkdir -p $(gcc_unpack_dir)
	cd $(gcc_unpack_dir) && \
		tar --strip-components=1 -xf $(gcc_src_dir)/gcc-*.tar.* && \
		patch -p1 < $(CURDIR)/local/patches/gcc.patch && \
		patch -p2 < $(gcc_src_dir)/patches/gcc-gfdl-build.diff && \
		rm -rf ./zlib/
	touch $@

$(stamp)gcc_%: $(stamp)binutils_% $(stamp)gcc_unpack
	mkdir -p $(gcc_build_dir)
	cd $(gcc_build_dir) && \
		$(gcc_unpack_dir)/configure \
			--build=$(DEB_BUILD_GNU_TYPE) \
			--host=$(DEB_BUILD_GNU_TYPE) \
			--target=$(target) \
			--with-system-zlib \
			--prefix=$(toolchain_dir) \
			--enable-languages=c \
			--disable-multilib \
			--disable-libffi \
			--disable-libgomp \
			--disable-libmudflap \
			--disable-libquadmath \
			--disable-libssp \
			--disable-nls \
			--disable-shared \
			--disable-plugins \
			--with-gnu-as \
			--with-gnu-ld \
			--with-headers=no \
			--without-newlib \
			--enable-build-warnings
	$(MAKE) -C $(gcc_build_dir) -j$(DEB_BUILD_OPTION_PARALLEL) all
	$(MAKE) -C $(gcc_build_dir) install
	touch $@
